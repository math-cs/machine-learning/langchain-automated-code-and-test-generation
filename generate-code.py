from langchain_openai import OpenAI
from langchain.prompts import PromptTemplate
from langchain.chains import LLMChain, SequentialChain
from dotenv import load_dotenv
import argparse

# Fetch OPENAI_API_KEY from .env file
load_dotenv()

# Command-line argument parsing
parser = argparse.ArgumentParser()
parser.add_argument(
    "--task", default="Check efficiently whether a given positive integer is prime")
parser.add_argument("--language", default="python")
args = parser.parse_args()

# Initialize OpenAI language model
llm = OpenAI()

# Define prompt templates for code and test
code_prompt = PromptTemplate(
    input_variables=["language", "task"],
    template="Write a very short {language} function that will {task}",
)

test_prompt = PromptTemplate(
    input_variables=["language", "code"],
    template="Write a {language} test for the following code:\n{code}"
)

# Create language model chains for code and test generation
code_chain = LLMChain(
    llm=llm,
    prompt=code_prompt,
    output_key="code"
)
test_chain = LLMChain(
    llm=llm,
    prompt=test_prompt,
    output_key="test"
)

# Create a sequential chain combining code and test generation
chain = SequentialChain(
    chains=[code_chain, test_chain],
    input_variables=["language", "task"],
    output_variables=["test", "code"]
)

# Generate code and test based on provided language and task
result = chain({
    "language": args.language,
    "task": args.task
})

# Print the generated code and test
print(">>>Generated Code:")
print(result["code"], "\n")
print(">>>Generated Test:")
print(result["test"])
